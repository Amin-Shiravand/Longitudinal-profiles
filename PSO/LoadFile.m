function [Inputs,Factors] = LoadFile()

[fileName,pathName] = uigetfile('*.xls');
if isequal(fileName,0)
   disp('User selected Cancel');
   return;
end
 filename1   = strcat(pathName ,fileName);

sheet2=2;
sheet3=3;
Inputs=xlsread(filename1,sheet2);
Factors=xlsread(filename1,sheet3);
[~, taskmsg] = system('tasklist|findstr "EXCEL.EXE"');
if ~isempty(taskmsg)
 system('taskkill /F /IM EXCEL.EXE');
  % If it gets here, shutting down worked, (I think) unless it asked them to save any unchanged work and they said to cancel shutdown.
end


