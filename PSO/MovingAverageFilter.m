function [NPVI,ggtZg] = MovingAverageFilter(ZGround,hs)
%%moving average filter
[envHigh, envLow] = envelope(ZGround(1,:),15,'peak');
envMean =(envHigh+envLow)/2;

gg=zeros(1,length(envMean)-1);
for i=1:length(envMean)-1
    gg(1,i)=(envMean(1,i+1)-envMean(1,i))/(hs(1,i+1)-hs(1,i));
end

ggtZg1=zeros(1,length(gg)-1);
for i=1:length(gg)-1
    if (gg(1,i)*gg(1,i+1)<0)
        ggtZg1(1,i)=i;
    end
end

for i=1:length(gg)-1
    if (gg(1,i)*gg(1,i+1)==0)
        ggtZg1(1,i)=i;
    end
end

ggtZg=find(ggtZg1>0);
NPVI=length(ggtZg);
