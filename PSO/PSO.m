clc;
clear;
close all;

[inputs,factors] = LoadFile();
hs(1,:)=inputs(:,1);
Zg(1,:)=inputs(:,2);
Zp(1,:)=inputs(:,3);
plot(hs,Zg);
hold on;

[NN,ggtZg]= MovingAverageFilter(Zg,hs);
[zmax,zmin]=GetZBoundary (Zg);
PVI(1,1)=factors(1,1);
PVI(1,NN+2)=factors(2,1);
KPVII=hs(1,ggtZg);
kmmax=max(hs);
KPVI=[0 KPVII kmmax];

%%moving average filter for Initialize Positions
iniZp=(Zp(1,ggtZg)-zmin)/(zmax-zmin);

%for i=1:NN
    %k(1,1)=zmax;
    %k(1,2)=zmin;
    %nn=1:1:2;
    %y=k(1,nn);
    %xx=KPVII(1,i);
    %%bar(xx,y(1,1),'EdgeColor',[0 .9 .9]);
    axis([0,kmmax,zmin,zmax+100]);
    hold on;
%end

%% Problem Definition

CostFunction=@costprofile;   % Cost Function

nVar=NN;                    % Number of Variables

VarSize=[1 nVar];           % Size of Variables Matrix

VarMin=0;                  % Lower Bound of Variables
VarMax=1;                  % Upper Bound of Variables

VarRange=[VarMin VarMax];   % Variation Range of Variables

VelMax=0.1;              % Maximum Velocity
VelMin=-0.1;             % Minimum Velocity

%% PSO-GA Parameters

MaxIt=100;          % Maximum Number of Iterations

nPop=8;            % Swarm (Population) Size

% Definition of Constriction Coefficients
phi1=2.05;
phi2=2.05;
phi=phi1+phi2;
chi=2/(phi-2+sqrt(phi^2-4*phi));
w=chi;
wdamp=1;        % Inertia Weight Damping Ratio
c1=2;
c2=2;


%% Initialization

empty_particle.Position=[];
empty_particle.Cost=[];
empty_particle.Velocity=[];
empty_particle.Best.Position=[];
empty_particle.Best.Cost=[];

particle=repmat(empty_particle,nPop,1);

GlobalBest.Cost.Total=inf;
GlobalBest.Cost.EarthWork=0;
GlobalBest.Cost.Bridge=0;
GlobalBest.Cost.Tunel=0;
GlobalBest.Cost.Penalty.Grade=0;
GlobalBest.Cost.Penalty.GradeTunel=0;
GlobalBest.Position=iniZp;

 % Initialize Position
    particle(1).Position=iniZp;
    % Initialize Velocity
    particle(1).Velocity=zeros(VarSize);
    
    % Evaluation
    [particle(1).Cost]=CostFunction(particle(1).Position,inputs,factors,NN,ggtZg,zmax,zmin);
    
    % Update Personal Best
    particle(1).Best.Position=particle(1).Position;
    particle(1).Best.Cost=particle(1).Cost;
    
    % Update Global Best
    if particle(1).Best.Cost<GlobalBest.Cost.Total
        
        GlobalBest.Cost.Total=particle(1).Best.Cost;
        GlobalBest.Position=particle(1).Best.Position;
        
    end


for i=2:nPop
    % Initialize Position
    particle(i).Position=unifrnd(VarMin,VarMax,VarSize);
    % Initialize Velocity
    particle(i).Velocity=zeros(VarSize);
    
    % Evaluation
    particle(i).Cost=CostFunction(particle(i).Position,inputs,factors,NN,ggtZg,zmax,zmin);
    
    % Update Personal Best
    particle(i).Best.Position=particle(i).Position;
    particle(i).Best.Cost=particle(i).Cost;
    
    % Update Global Best
    if particle(i).Best.Cost<GlobalBest.Cost.Total
        
        GlobalBest.Cost.Total=particle(i).Best.Cost;
        GlobalBest.Position=particle(i).Best.Position;
        
    end
    
end

BestCost=zeros(MaxIt,1);

nfe=zeros(MaxIt,1);

    
%% PSO Main Loop

for it=1:MaxIt
    
    for i=1:nPop
        
        % Update Velocity
        particle(i).Velocity = w*particle(i).Velocity ...
            +c1*rand(VarSize).*(particle(i).Best.Position-particle(i).Position) ...
            +c2*rand(VarSize).*(GlobalBest.Position-particle(i).Position);
        
        % Apply Velocity Limits
        particle(i).Velocity = max(particle(i).Velocity,VelMin);
        particle(i).Velocity = min(particle(i).Velocity,VelMax);
        
        % Update Position
        particle(i).Position = particle(i).Position + particle(i).Velocity;
        
        % Velocity Mirror Effect
        IsOutside=(particle(i).Position<VarMin | particle(i).Position>VarMax);
        particle(i).Velocity(IsOutside)=-particle(i).Velocity(IsOutside);
        
        % Apply Position Limits
        particle(i).Position = max(particle(i).Position,VarMin);
        particle(i).Position = min(particle(i).Position,VarMax);

        % % Evaluation
        % [particle(i).Cost,GlobalBest.Cost.EarthWork,GlobalBest.Cost.Bridge,GlobalBest.Cost.Tunel,...
         % GlobalBest.Cost.Penalty.Grade,...
         % GlobalBest.Cost.Penalty.GradeTunel] = CostFunction(particle(i).Position,inputs,factors,NN,ggtZg,zmax,zmin);
		[particle(i).Cost,TempEarthWork,TempBridge,TempTunel,...
         TempGrade,...
         TempGradeTunelGradeTunel] = CostFunction(particle(i).Position,inputs,factors,NN,ggtZg,zmax,zmin);
        
        % Update Personal Best
        if particle(i).Cost<particle(i).Best.Cost
            
            particle(i).Best.Position=particle(i).Position;
            particle(i).Best.Cost=particle(i).Cost;
            
            % Update Global Best
            if particle(i).Best.Cost<GlobalBest.Cost.Total
                
				GlobalBest.Cost.Total=particle(i).Best.Cost;
				GlobalBest.Position=particle(i).Best.Position;
				GlobalBest.Cost.EarthWork = TempEarthWork;
				GlobalBest.Cost.Bridge= TempBridge;
				GlobalBest.Cost.Tunel = TempTunel;
				GlobalBest.Cost.Penalty.Grade=TempGrade;
				GlobalBest.Cost.Penalty.GradeTunel=TempGradeTunelGradeTunel;
                
            end
            
        end
        
    end
    
    BestCost(it)=GlobalBest.Cost.Total;
    
%     nfe(it)=NFE;
    
    disp(['Iteration ' num2str(it)  ', Best Cost = ' num2str(BestCost(it))]);
    
    w=w*wdamp;
    
end

%% Results

figure;
plot(1:MaxIt,BestCost,'LineWidth',2);
xlabel('iteration');
ylabel('Best Cost');

figure;
for i=1:NN
    PVI(1,i+1)=GlobalBest.Position(1,i)*(zmax-zmin)+zmin;
end

plot(hs,Zg);
hold on;
plot(KPVI(:),PVI(:),'r','Linewidth',1);
hold on;

