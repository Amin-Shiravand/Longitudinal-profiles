clc;
clear;
close all;

[inputs,factors] = LoadFile();

hs(1,:)=inputs(:,1);
Zg(1,:)=inputs(:,2);
Zp(1,:)=inputs(:,3);
plot(hs,Zg);

[NN,ggtZg]= MovingAverageFilter(Zg,hs);
[zmax,zmin]=GetZBoundary (Zg);
PVI(1,1)=factors(1,1);
PVI(1,NN+2)=factors(2,1);
KPVII=hs(1,ggtZg);
kmmax=max(hs);
KPVI=[0 KPVII kmmax];


%%moving average filter for Initialize Positions
iniZp=(Zp(1,ggtZg)-zmin)/(zmax-zmin);

%for i=1:NN
    %k(1,1)=zmax;
    %k(1,2)=zmin;
    %nn=1:1:2;
    %y=k(1,nn);
    %xx=KPVII(1,i);
    %%bar(xx,y(1,1),'EdgeColor',[0 .9 .9]);
    axis([0,kmmax,zmin,zmax+100]);
    hold on;
%end

%% Problem Definition

CostFunction=@costprofile;       % Cost Function

nVar=NN;                    % Number of Variables

VarSize=[1 nVar];           % Size of Variables Matrix

VarMin=0;                  % Lower Bound of Variables
VarMax=1;                  % Upper Bound of Variables

VarRange=[VarMin VarMax];   % Variation Range of Variables

VelMax=(VarMax-VarMin)/10;  % Maximum Velocity
VelMin=-VelMax;             % Minimum Velocity

%% PSO-GA Parameters

MaxIt=100;          % Maximum Number of Iterations

MaxSubItGA=1;       % Maximum Number of Sub-Iterations for GA

MaxSubItPSO=1;      % Maximum Number of Sub-Iterations for PSO

nPop=5;            % Swarm (Population) Size

% Definition of Constriction Coefficients
phi1=2.05;
phi2=2.05;
phi=phi1+phi2;
chi=2/(phi-2+sqrt(phi^2-4*phi));

w=chi;
c1=phi1*chi;
c2=phi2*chi;

pCrossover=0.7;                         % Crossover Percentage
nCrossover=round(pCrossover*nPop/2)*2;  % Number of Parents (Offsprings)

pMutation=0.2;                      % Mutation Percentage
nMutation=round(pMutation*nPop);    % Number of Mutants


%% Initialization

% Empty Structure to Hold Individuals Data
empty_individual.Position=[];
empty_individual.Velocity=[];
empty_individual.Cost=[];
empty_individual.Best.Position=[];
empty_individual.Best.Cost=[];

% Create Population Matrix
pop=repmat(empty_individual,nPop,1);
% Global Best
GlobalBest.Cost.Total=inf;
GlobalBest.Cost.EarthWork=0;
GlobalBest.Cost.Bridge=0;
GlobalBest.Cost.Tunel=0;
GlobalBest.Cost.Penalty.Grade=0;
GlobalBest.Cost.Penalty.GradeTunel=0;
GlobalBest.Position=iniZp;

 % Initialize Position
pop(1).Position=iniZp;
 % Initialize Velocity
pop(1).Velocity=zeros(VarSize);
    
 % Evaluation
 pop(1).Cost=CostFunction(pop(1).Position,inputs,factors,NN,ggtZg,zmax,zmin);
    
 % Update Personal Best
pop(1).Best.Position=pop(1).Position;
pop(1).Best.Cost=pop(1).Cost;
    
    % Update Global Best
    if pop(1).Best.Cost<GlobalBest.Cost.Total
        GlobalBest.Cost.Total=pop(1).Best.Cost;
        GlobalBest.Position=pop(1).Best.Position;
    end

for i=2:nPop
    % Initialize Position
    pop(i).Position=unifrnd(VarMin,VarMax,VarSize);
    % Initialize Velocity
    pop(i).Velocity=zeros(VarSize);
    
    % Evaluation
    pop(i).Cost=CostFunction(pop(i).Position,inputs,factors,NN,ggtZg,zmax,zmin);
    
    % Update Personal Best
    pop(i).Best.Position=pop(i).Position;
    pop(i).Best.Cost=pop(i).Cost;
    
       % Update Global Best
    if pop(i).Best.Cost<GlobalBest.Cost.Total
        GlobalBest.Cost.Total=pop(i).Best.Cost;
        GlobalBest.Position=pop(i).Best.Position;
    end
    
end

% Sort Population
pop=SortPopulation(pop);

% Vector to Hold Best Cost Values
BestCost=zeros(MaxIt,1);

%% PSO-GA Main Loop

for it=1:MaxIt
    % PSO Operators
    for psoit=1:MaxSubItPSO
        for i=1:nPop

            % Update Velocity
            pop(i).Velocity=w*pop(i).Velocity ...
                + c1*rand(VarSize).*(pop(i).Best.Position-pop(i).Position) ...
                + c2*rand(VarSize).*(GlobalBest.Position-pop(i).Position);

            % Apply Velocity Bounds
            pop(i).Velocity=min(max(pop(i).Velocity,VelMin),VelMax);
            % Update Position
            pop(i).Position=pop(i).Position+pop(i).Velocity;

            % Velocity Reflection
            flag=(pop(i).Position<VarMin | pop(i).Position>VarMax);
            pop(i).Velocity(flag)=-pop(i).Velocity(flag);

            % Apply Position Bounds
            pop(i).Position=min(max(pop(i).Position,VarMin),VarMax);

       
           [pop(i).Cost,TempEarthWork,TempBridge,TempTunel,...
            TempGrade,...
            TempGradeTunelGradeTunel] = CostFunction(pop(i).Position,inputs,factors,NN,ggtZg,zmax,zmin);
            
            % Update Personal Best
            if pop(i).Cost<pop(i).Best.Cost
                 pop(i).Best.Position=pop(i).Position;
                pop(i).Best.Cost=pop(i).Cost;
            
            % Update Global Best
            if pop(i).Best.Cost<GlobalBest.Cost.Total
                GlobalBest.Cost.Total=pop(i).Best.Cost;
				GlobalBest.Position=pop(i).Best.Position;
				GlobalBest.Cost.EarthWork = TempEarthWork;
				GlobalBest.Cost.Bridge= TempBridge;
				GlobalBest.Cost.Tunel = TempTunel;
				GlobalBest.Cost.Penalty.Grade=TempGrade;
				GlobalBest.Cost.Penalty.GradeTunel=TempGradeTunelGradeTunel;
            end
            end

        end
    end
    
    % GA Operators
    for gait=1:MaxSubItGA
        % Crossover
        popc=repmat(empty_individual,nCrossover/2,2);
        for k=1:nCrossover/2

            i1=randi([1 nPop]);
            i2=randi([1 nPop]);

            p1=pop(i1);
            p2=pop(i2);

            [popc(k,1).Position, popc(k,2).Position]=Crossover(p1.Position,p2.Position,VarRange);

            popc(k,1).Cost=CostFunction(popc(k,1).Position,inputs,factors,NN,ggtZg,zmax,zmin);
            popc(k,2).Cost=CostFunction(popc(k,2).Position,inputs,factors,NN,ggtZg,zmax,zmin);

            if p1.Best.Cost<p2.Best.Cost
                popc(k,1).Best=p1.Best;
                popc(k,2).Best=p1.Best;
            else
                popc(k,1).Best=p2.Best;
                popc(k,2).Best=p2.Best;
            end

            if rand<0.5
                popc(k,1).Velocity=p1.Velocity;
                popc(k,2).Velocity=p2.Velocity;
            else
                popc(k,1).Velocity=p2.Velocity;
                popc(k,2).Velocity=p1.Velocity;
            end

        end
        popc=popc(:);


        % Mutation
        popm=repmat(empty_individual,nMutation,1);
        for k=1:nMutation

            i=randi([1 nPop]);

            p=pop(i);

            popm(k).Position=Mutate(p.Position,VarRange);

            popm(k).Cost=CostFunction(popm(k).Position,inputs,factors,NN,ggtZg,zmax,zmin);

            popm(k).Velocity=p.Velocity;

            popm(k).Best=p.Best;

        end

        % Merge Population
        pop=[pop
             popc
             popm];

        % Sort Population
        pop=SortPopulation(pop);

            [pop(1).Cost,TempEarthWork,TempBridge,TempTunel,...
            TempGrade,...
            TempGradeTunelGradeTunel] = CostFunction(pop(1).Position,inputs,factors,NN,ggtZg,zmax,zmin);
     
            % Update Global Best
            if pop(1).Best.Cost<GlobalBest.Cost.Total
                GlobalBest.Cost.Total=pop(1).Best.Cost;
				GlobalBest.Position=pop(1).Best.Position;
				GlobalBest.Cost.EarthWork = TempEarthWork;
				GlobalBest.Cost.Bridge= TempBridge;
				GlobalBest.Cost.Tunel = TempTunel;
				GlobalBest.Cost.Penalty.Grade=TempGrade;
				GlobalBest.Cost.Penalty.GradeTunel=TempGradeTunelGradeTunel;
            end   

   
    end
    
    % Store Best Cost
    BestCost(it)=GlobalBest.Cost.Total;
    
    % Show Iteration Information
    disp(['Iteration ' num2str(it) ': Best Cost = ' num2str(BestCost(it))]);
    
end

%% Plots

figure;
plot(1:MaxIt,BestCost,'LineWidth',2);
xlabel('iteration');
ylabel('Best Cost');


