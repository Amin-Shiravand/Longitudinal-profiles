function[zmax,zmin] =GetZBoundary (Zg)
zmax=max(Zg)-100;
zmin=min(Zg);