function [JT,Jew,Jpol,Jton,Cg,CgT]=costprofile(Unif,inputs,factors,NPVI,ggtZg,zmax,zmin)

hs(1,:)=inputs(:,1);
Zg(1,:)=inputs(:,2);

NN=NPVI;
PVI(1,1)=factors(1,1);
PVI(1,NN+2)=factors(2,1);
KPVII=hs(1,ggtZg);
kmmax=max(hs);
KPVI=[0 KPVII kmmax];

for i=1:NN
    PVI(1,i+1)=Unif(1,i)*(zmax-zmin)+zmin;
end
plot(KPVI(:),PVI(:),'r','Linewidth',1);
%%hold on;

%%���?� ��� ����
K1=factors(4,1);     %k ��� ����
K2=factors(5,1);     %k ��� ���� 

G=zeros(1,NN+1);
for i=2:NN+2
    G(1,i-1)=(PVI(1,i)-PVI(1,i-1))/(KPVI(1,i)-KPVI(1,i-1));    %�?� ���?
end

A=zeros(1,NN);
AA=zeros(1,NN);
LPVI=zeros(1,NN);
PVC=zeros(1,NN);
PVT=zeros(1,NN);
for i=1:NN
    A(1,i)=abs(G(1,i+1)-G(1,i));
    AA(1,i)=G(1,i+1)-G(1,i);
    if AA(1,i)<0
        LPVI(1,i)=K1*A(1,i)*100;           %L ��� ��� ���� ����
    else
        LPVI(1,i)=K2*A(1,i)*100;           %L ��� ��� ���� ����
    end
    if LPVI(1,i)<30
       LPVI(1,i)=30;
    end
    PVC(1,i)=KPVI(1,i+1)-(LPVI(1,i)/2);
    PVT(1,i)=KPVI(1,i+1)+(LPVI(1,i)/2);
end

for i=2:NN+1
    PVC(2,i-1)=PVI(1,i)-(G(1,i-1)*(LPVI(1,i-1)/2));
    PVT(2,i-1)=PVI(1,i)+(G(1,i)*(LPVI(1,i-1)/2));
end


 a1=zeros(1,NN);
 b1=zeros(1,NN);
 c1=zeros(1,NN);
for i=1:NN
    a1(1,i)=(AA(1,i))/(2*LPVI(1,i));
    b1(1,i)=((G(1,i+1)+G(1,i))/2)-(KPVI(1,i+1)*(AA(1,i))/LPVI(1,i));
    c1(1,i)=PVI(1,i+1)-(a1(1,i)*(KPVI(1,i+1)^2+(LPVI(1,i)^2)/4))-(b1(1,i)*KPVI(1,i+1))+(LPVI(1,i)*(AA(1,i))/4);
end


 deltaLPVI=zeros(1,NN);
 LPVIC=zeros(NN,10);
 ZPVIC=zeros(NN,10);
for i=1:NN
    deltaLPVI(1,i)=(LPVI(1,i)/10);
    for k=1:10
        LPVIC(i,k)=PVC(1,i)+k*deltaLPVI(1,i);
        LPVIC(i,1)=PVC(1,i);
        ZPVIC(i,k)=a1(1,i)*(LPVIC(i,k)^2)+b1(1,i)*LPVIC(i,k)+c1(1,i);
        ZPVIC(i,1)=a1(1,i)*(PVC(1,i)^2)+b1(1,i)*PVC(1,i)+c1(1,i);
    end
end 

for i=1:NN
    plot(LPVIC(i,1:10),ZPVIC(i,1:10),'g');           %���?� ��� ����
   %% hold on;
end

%%���� ��?��
%%�� ?��� �� �?��?�� ��

tetarizi=factors(15,1);
tetabardari=factors(16,1);
Lw=factors(8,1);
Zr=zeros(numel(Zg),1);
for i=2:NN
    for j=1:numel(Zg)
        if (KPVI(1,1)<=hs(1,j) && hs(1,j)<=PVC(1,1))
           Zr(j,1)=PVI(1,1)+G(1,1)*hs(1,j);    
        elseif (PVT(1,i-1)<=hs(1,j) && hs(1,j)<=PVC(1,i))
            Zr(j,1)=PVT(2,i-1)+G(1,i)*(hs(1,j)-PVT(1,i-1));
         elseif (PVT(1,NN)<=hs(1,j) && hs(1,j)<=KPVI(1,NN+2))
            Zr(j,1)=PVT(2,NN)+G(1,NN+1)*(hs(1,j)-PVT(1,NN)); 
        elseif (PVC(1,i-1)<hs(1,j) && hs(1,j)<PVT(1,i-1))
            Zr(j,1)=a1(1,i-1)*((hs(1,j))^2)+b1(1,i-1)*(hs(1,j))+c1(1,i-1);      
        elseif (PVC(1,NN)<hs(1,j) && hs(1,j)<PVT(1,NN))
            Zr(j,1)=a1(1,NN)*(hs(1,j)^2)+b1(1,NN)*(hs(1,j))+c1(1,NN);
        end
    end
end

Ac=zeros(numel(Zg));
Af=zeros(numel(Zg));
for i=1:numel(Zg)
    if Zg(1,i)>Zr(i,1)
        Ac(i,1)=0.5*(Zg(1,i)-Zr(i,1))*Lw*(Lw+(2*(Zg(1,i)-Zr(i,1))*tetarizi/100));
    else
        Ac(i,1)=0;
    end
     if Zr(i,1)>Zg(1,i)
        Af(i,1)=0.5*(Zr(i,1)-Zg(1,i))*Lw*(Lw+(2*(Zr(i,1)-Zg(1,i))*tetabardari/100));
    else
        Af(i,1)=0;
     end
end


%%4-��?�� ?����? � �������?

hsspol=zeros(numel(Zg),1);
hsstoonel=zeros(numel(Zg),1);
Apol=zeros(numel(Zg),1);
Atoonel=zeros(numel(Zg),1);
for i=1:numel(Zg)
    if (Zr(i,1)-Zg(1,i))>15
        Af(i,1)=0;
        Apol(i,1)=1;
        hsspol(i,1)=hs(1,i);
    else 
        Apol(i,1)=0;
        hsspol(i,1)=0;
    end
    if (Zg(1,i)-Zr(i,1))>20
        Ac(i,1)=0;
        Atoonel(i,1)=1;
        hsstoonel(i,1)=hs(1,i);
    else 
        Atoonel(i,1)=0;
        hsstoonel(i,1)=0;
    end
end

kmendpol=zeros(0);
kmstartpol=zeros(0);
for i=1:numel(Zg)-1
    if (hsspol(i,1)~=0 && hsspol(i+1,1)==0)
        kmendpol= [kmendpol,i];   
    end
    if (hsspol(i,1)==0 && hsspol(i+1,1)~=0)
        kmstartpol=[kmstartpol, i+1];
    end
end

kmendtoonel=zeros(0);
kmstarttoonel=zeros(0);
for i=1:numel(Zg)-1
    if (hsstoonel(i,1)~=0 && hsstoonel(i+1,1)==0)
        kmendtoonel=[kmendtoonel,i];
    end
    if (hsstoonel(i,1)==0 && hsstoonel(i+1,1)~=0)
        kmstarttoonel=[kmstarttoonel,i+1];
    end
end

endtoonel=transpose(kmendtoonel);
starttoonel=transpose(kmstarttoonel);
endpol= transpose(kmendpol);
startpol=transpose(kmstartpol);

%% Old code for finding  start and end of bridge And Tunnel
% kmendpol=zeros(1,numel(Zg)-1);
% kmstartpol=zeros(1,numel(Zg)-1);
% for i=1:numel(Zg)-1
%     if (hsspol(i,1)~=0 && hsspol(i+1,1)==0)
%         kmendpol(i,1)=hsspol(i,1);
%     end
%     if (hsspol(i,1)==0 && hsspol(i+1,1)~=0)
%         kmstartpol(i,1)=hsspol(i+1,1);
%     end
% end
% endpol=find(kmendpol>0);
% startpol=find(kmstartpol>0)+1;

% kmendtoonel=zeros(1,numel(Zg)-1);
% kmstarttoonel=zeros(1,numel(Zg)-1);
% for i=1:numel(Zg)-1
%     if (hsstoonel(i,1)~=0 && hsstoonel(i+1,1)==0)
%         kmendtoonel(i,1)=hsstoonel(i,1);
%     end
%     if (hsstoonel(i,1)==0 && hsstoonel(i+1,1)~=0)
%         kmstarttoonel(i,1)=hsstoonel(i+1,1);
%     end
% end
% 
% endtoonel=find(kmendtoonel>0);
% starttoonel=find(kmstarttoonel>0)+1;

LLpol=zeros(numel(endpol),1);
for i=1:numel(endpol)
    LLpol(i,1)=(hs(endpol(i,1))-hs(startpol(i,1)));
end

LLtoonel=zeros(numel(endtoonel),1);
for i=1:numel(endtoonel)
    LLtoonel(i,1)=(hs(endtoonel(i,1))-hs(starttoonel(i,1)));
end

Lpol=sum(LLpol);
Ltoonel=sum(LLtoonel);

Jpol=Lw*Lpol*factors(9,1);
Jton=Ltoonel*factors(10,1);


%%������ ��� ���?�� �ǘ?
Pew1=factors(21,1);
Pew2=factors(22,1);
Pew3=factors(19,1);
Pew5=factors(18,1);

Vcc=zeros(numel(Zg),1);
Vff=zeros(numel(Zg),1);
 for i=2:numel(Zg)
     if Ac(i,1)>0 && Ac(i-1,1)>=0
         Vcc(i,1)=0.5*(Ac(i,1)+Ac(i-1,1))*(hs(1,i)-hs(1,i-1));
     else
        Vcc(i,1)=0;
     end
     if Af(i,1)>0 && Af(i-1,1)>=0
         Vff(i,1)=0.5*(Af(i,1)+Af(i-1,1))*(hs(1,i)-hs(1,i-1));
     else
        Vff(i,1)=0;
     end
 end

Vf=sum(Vff);
Vc=sum(Vcc);
EN=Vc*Pew5-Vf;

Jew=(Vc*Pew1)+(Vf*Pew2)+EN*Pew3;

%%Penalty Function
%%�?� ���?
Gmax=factors(3,1);
Gmin=factors(6,1);
GmaxT=factors(7,1);

CG1=factors(24,1);
CG2=factors(25,1);
CG3=factors(26,1);
CG4=factors(27,1);
CG5=factors(28,1);
CG6=factors(29,1);

for i=2:NN+2
    G(1,i-1)=(PVI(1,i)-PVI(1,i-1))/(KPVI(1,i)-KPVI(1,i-1));    %�?� ���?
end

kstarttoonel=zeros(numel(endtoonel),1);
kendtoonel=zeros(numel(endtoonel),1);
for i=1:numel(endtoonel)
    kstarttoonel(i,1)=hs(starttoonel(i,1));
    kendtoonel(i,1)=hs(endtoonel(i,1));
end

CGT=zeros(1,numel(KPVI));
for i=1:numel(endtoonel)
    for j=2:NN+2
        if (KPVI(1,j-1)>=kstarttoonel(i,1) && KPVI(1,j)<=kendtoonel(i,1) && abs(G(1,j-1))>GmaxT ) 
            CGT(1,j-1)=CG5*((abs(G(1,j-1))-GmaxT)*100)^CG6;
        end
    end
end

CgT=sum(CGT);


CG=zeros(1,NN+1);
for i=1:NN+1
    
    if abs(G(1,i))>Gmax
       CG(1,i)= CG1*((abs(G(1,i))-Gmax)*100)^CG2;
    elseif abs(G(1,i))<Gmin
         CG(1,i)= CG3*((Gmin-abs(G(1,i)))*100)^CG4;         
    else   
         CG(1,i)=0;
    end
   
end

Cg=sum(CG);

   
%%���� ��?��
JT=Jew+Jpol+Jton+Cg+CgT;
end

 
