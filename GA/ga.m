clc;
clear;
close all;

[inputs,factors] = LoadFile();
hs(1,:)=inputs(:,1);
Zg(1,:)=inputs(:,2);
Zp(1,:)=inputs(:,3);
plot(hs,Zg);

[NN,ggtZg]= MovingAverageFilter(Zg,hs);
[zmax,zmin]=GetZBoundary (Zg);
PVI(1,1)=factors(1,1);
PVI(1,NN+2)=factors(2,1);
KPVII=hs(1,ggtZg);
kmmax=max(hs);
KPVI=[0 KPVII kmmax];


%%moving average filter for Initialize Positions
iniZp=(Zp(1,ggtZg)-zmin)/(zmax-zmin);

%for i=1:NN
    %k(1,1)=zmax;
    %k(1,2)=zmin;
    %nn=1:1:2;
    %y=k(1,nn);
    %xx=KPVII(1,i);
    %%bar(xx,y(1,1),'EdgeColor',[0 .9 .9]);
    axis([0,kmmax,zmin,zmax+100]);
    hold on;
%end

%% Problem Definition

CostFunction=@costprofile;       % Cost Function

nVar=NN;                    % Number of Variables

VarSize=[1 nVar];           % Size of Variables Matrix

VarMin=0;                  % Lower Bound of Variables
VarMax=1;                  % Upper Bound of Variables

VarRange=[VarMin VarMax];   % Variation Range of Variables

VelMax=(VarMax-VarMin)/10;  % Maximum Velocity
VelMin=-VelMax;             % Minimum Velocity

%% GA Parameters

MaxIt=100;      % Maximum Number of Iterations

nPop=10;        % Population Size

pCrossover=0.7;                         % Crossover Percentage
nCrossover=round(pCrossover*nPop/2)*2;  % Number of Parents (Offsprings)

pMutation=0.2;                      % Mutation Percentage
nMutation=round(pMutation*nPop);    % Number of Mutants


%% Initialization

% Empty Structure to Hold Individuals Data
empty_individual.Position=[];
empty_individual.Cost=[];

% Global Best
% Create Population Matrix
pop=repmat(empty_individual,nPop,1);

% Initialize Positions

pop(1).Position=iniZp;
pop(1).Cost=CostFunction(pop(1).Position,inputs,factors,NN,ggtZg,zmax,zmin);

for i=2:nPop
    pop(i).Position=unifrnd(VarMin,VarMax,VarSize);
    pop(i).Cost=CostFunction(pop(i).Position,inputs,factors,NN,ggtZg,zmax,zmin);
end

% Sort Population
pop=SortPopulation(pop);

% Store Best Solution
BestSol=pop(1);

% Vector to Hold Best Cost Values
BestCost=zeros(MaxIt,1);

%% GA Main Loop

for it=1:MaxIt
    
    % Crossover
    popc=repmat(empty_individual,nCrossover/2,2);
    for k=1:nCrossover/2
        
        i1=randi([1 nPop]);
        i2=randi([1 nPop]);
        
        p1=pop(i1);
        p2=pop(i2);
        
        [popc(k,1).Position popc(k,2).Position]=Crossover(p1.Position,p2.Position,VarRange);
        
        popc(k,1).Cost=CostFunction(popc(k,1).Position,inputs,factors,NN,ggtZg,zmax,zmin);
        popc(k,2).Cost=CostFunction(popc(k,2).Position,inputs,factors,NN,ggtZg,zmax,zmin);
        
    end
    popc=popc(:);
    
    
    % Mutation
    popm=repmat(empty_individual,nMutation,1);
    for k=1:nMutation
        
        i=randi([1 nPop]);
        
        p=pop(i);
        
        popm(k).Position=Mutate(p.Position,VarRange);
        
        popm(k).Cost=CostFunction(popm(k).Position,inputs,factors,NN,ggtZg,zmax,zmin);
        
    end
    
    % Merge Population
    pop=[pop
         popc
         popm];
    
    % Sort Population
    pop=SortPopulation(pop);
    
    % Delete Extra Individuals
    pop=pop(1:nPop);
    
    % Update Best Solution
    BestSol=pop(1);
    
    % Store Best Cost
    BestCost(it)=BestSol.Cost;
    
    % Show Iteration Information
    disp(['Iteration ' num2str(it) ': Best Cost = ' num2str(BestCost(it))]);
    
end

%% Plots

figure;
semilogy(BestCost);


